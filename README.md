# Agile: User Story & Story Points
Agile is an iterative approach to project management and software development that helps teams deliver value to their customers faster and with fewer headaches. Instead of betting everything on a "big bang" launch, an agile team delivers work in small, but consumable, increments. Requirements, plans, and results are evaluated continuously so teams have a natural mechanism for responding to change quickly.

## User stories
User Stories should be discussed with Developer, Scrum Master and Product owner to define the acceptance criteria. Typically, a user story should be covered in one or two development cycle, if not, it should be split into multiple user stories.

**User Story format:**   
```
As a ... {Who}
I want ... {What}
So that ... {Why}
```

Example: As a *user* I want *to log-in* so that *I can access the application*. 

**Acceptance Criteria format:**
```
Given ... {A context}
When ... {An event}
Then ... {An outcome}
```

Example: Given *for an application user* when *he successfully logs in*, then *lead to the home page of application*.

**User Story Key Points:**
- User oriented requirements.
- Less detailed.
- Faster in development, ability to complete within development cycle.
- Right fit for Agile methodology.
- User stories are often written on index cards or sticky notes arranges on walls or tables.
- Arrangement in a logical/prioritized way which is know as user story mapping.
- This facilitates planning and discussion.

**User story refinement**:   
Refining user stories is a process of revisiting the details and clarity of each user story, whereas product refinement is a higher level process of prioritising user stories.

The purpose of the user stories is to ensure that intent of the user is correctly expressed in language that a developer and tester can fully understand.  The stories need to be simple, clear, unambiguous and consistent, with nothing missing.  By refining your stories, you will eliminate many requirements defects that are often not found until well after the design and development work has started.

**Product refinement**:   
When the product owner and some, or all, of the rest of the team review items on the backlog to ensure the backlog contains the appropriate items, that they are prioritised, and that the items at the top of the backlog are ready for delivery. This activity occurs on a regular basis and may be an officially scheduled meeting or an ongoing activity. Some activities that occur during this refinement of the backlog include:

### Definition of Ready (DoR)
User story must meet the following criteria before being accepted into an upcoming Sprint. A product owner is responsible for the DoR.

**Criteria:**
- A clear description
- Acceptance criteria
- Priority defined
- Ready to work on (Story points assigned)

**Advantages:**
- It relives the team from working on partially implemented or partially complete acceptance criteria.
- It also relives costly back-and-forth discussion or re-work for the development team.

### Definition of Done (DoD)
The list of criteria must be met before a product increment (user story) is considered "done"

**Criteria**  
When the team complete the following outline actions by the end of the sprint.
- Programming is complete.
- Fully tested / integrated.
- Test cases / test documents prepares.
- Appropriate documentation is done.
- Ready to be shipped.

*Done means, not just "programming finished"; but "ready to ship".*

### Real life example
<table>
  <thead>
    <tr>
      <th>Epics</th>
      <th>List - Browse</th>
      <th>Order</th>
      <th>Pay</th>
      <th>Track</th>
      <th>Feedback</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5">User Stories</th>
      <td>List the menu items</td>
      <td>Registration</td>
      <td>Cash CoD</td>
      <td>Estimated arrival time</td>
      <td>Feedback entry</td>
    </tr>
    <tr>
      <td>Keyword search</td>
      <td>Login</td>
      <td>Debit/Credit</td>
      <td>Support</td>
      <td>Response</td>
    </tr>
    <tr>
      <td>ADMIN: add/delete menu items</td>
      <td>Add/remove cart item</td>
      <td>Internet banking</td>
      <td>Order status</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td>Customer order</td>
      <td>PayPal</td>
      <td>Track online</td>
      <td>Compliments</td>
    </tr>
    <tr>
      <td></td>
      <td>Repeat previous order</td>
      <td>Other payments</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

Requirement list: Features / User Stories

## Story Points
Story points are a metric used in Agile management to determine (estimate) the difficulty of implementing the User Story.
- Agile teams prefer to express the estimates in relative units rather than in time (hours or day).
- It is an art of categorizing User Stories into different 'relative' sizes.
- Story points relieve the tension and **difficulty involved in absolute calculation** of project estimations.

Import facts about Story Points:
- Story points are not related to efforts.
- It is just a relative number assigned based on size (units are not important).
- Big user stories should be broken into small, manageable sizes (completion within a sprint).

### Fibonacci sequence
In agile, the Fibonacci sequence is used to represent the size of the User Story. Based on that, the story points should always fall into a fibonacci number.
```
1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144..
```
During the product grooming exercise, the development team take the user story and discuss detail about it. The functionality, it's size, complexity and other factors. Once they understand everything about the user story, each team member will assign a number to the user story. This is **NOT** about the time or effort required to complete the user story but indirectly describes the magnitude or size of the user story. 

At the end, a cross-check how many story points are assigned by each developer will take place. If there is any major difference between the team members, they will again, discuss the story and come to a common conclusion.

**Example:** One team member assigns 34 points to a User Story, another team member thinks that User Story is more than 34 points. In this case, you may not assign a number like 35, 36 or 37. It will be assigned as 55 points.

## Sizing & Estimation
In agile, we first figure out if the user story is SMALL, BIG or VERY BIG. You are not trying to find an absolute value. If the size is found to be very big or big, it should be broken up into smaller more manageable sizes. Which makes it easier to estimate, easier to develop, and easier to test.

### Levels of Estimation
- **Overall project or proposal level:**
    - At this level, a quick, functional, analysis, is done during the initial phase of the Project/Product development.
    - Large Epics are estimated by techniques like: **Bucket Sizing** and **T-Shirt sizing**.
- **Release level:**
    - Epics, large User Stories are broken into smaller user stories.
    - The story points are assigned to the user stories based on size & complexity.
    - Decide which stories can be taken in current release and which can be taken later. (**Prioritization**)
    - User stories are estimated based on techniques like (**Planning poker**)
- **Sprint level:**
    - User stories are broken down into user tasks.
    - Estimated hours are assigned to these tasks according to their complexity / technology.
    - **This information can later be used to calculate a somewhat precise level of budge for an Agile project**
  
### T-Shirt Sizing
This is a perfect technique to give a rough estimation of the large items in the product backlog. This approach is similar to t-shirt how t-shirts are sized (e.g., XS, S, M, L, XL). This technique is useful when a quick and rough estimation is needed; later these can be converted into smaller items.

A relative size (usually Medium) is decided **after a mutual discussion and agreement of the team members**. 

**Disadvantage**: One disadvantage to this technique is what seems L to someone may seem to be XL for someone else.

### Planning Poker
As a team, a user story will be chosen from the **backlog** and discussed briefly. Each member will mentally formulate an estimate and write their number on a card (Based on Fibonacci sequence), then everyone reveals their estimation to the team. If everyone is in agreement on a number, great. If not, discuss the rationale behind the extreme estimates. (min & max values). Get a consensus among the team and decide on an agreed upon number.

Since product backlog items will continually to be added, a planning poker for each Sprint is suggested.
